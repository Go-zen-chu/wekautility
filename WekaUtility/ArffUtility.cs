﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using weka.core;
using weka.core.converters;
using weka.filters;
using weka.filters.unsupervised.attribute;

namespace WekaUtility
{
    public static class ArffUtility
    {
        public static Instances DeepClone(this Instances instances)
        {
            var attrNum = instances.numAttributes();
            var instNum = instances.numInstances();
            var attrVector = new FastVector(attrNum);
            for (int attrIdx = 0; attrIdx < attrNum; attrIdx++)
            {
                attrVector.addElement(instances.attribute(attrIdx));
            }
            // weka doesnt deep copy attributes
            var newInsts = new Instances(instances.relationName(), attrVector, instNum);
            newInsts.setClassIndex(instances.classIndex());
            for (int i = 0; i < instNum; i++)
            {
                var inst = instances.instance(i);
                newInsts.add(new Instance(inst));
            }
            return newInsts;
        }

        /// <summary>
        /// Load Arff file from path
        /// </summary>
        /// <param name="arffFilePath"></param>
        /// <returns></returns>
        public static Instances LoadArffFile(string arffFilePath, int classIdx = -1)
        {
            using(var fr = new java.io.FileReader(arffFilePath))
            using (var br = new java.io.BufferedReader(fr))
            {
                var instances = new weka.core.Instances(br);
                instances.setClassIndex(classIdx < 0 ? instances.numAttributes() - 1 : classIdx);
                return instances;
            }
        }
        /// <summary>
        /// Load Arff files and concanate to one instances
        /// </summary>
        /// <param name="arffFilePaths"></param>
        /// <returns></returns>
        public static Instances LoadArffFiles(IEnumerable<string> arffFilePaths)
        {
            return ConcatenateInstances(arffFilePaths.Select(path => LoadArffFile(path)));
        }
        public static Instances LoadArffFiles(string arffDirPath)
        {
            if (Directory.Exists(arffDirPath) == false) throw new DirectoryNotFoundException(arffDirPath);
            var arffFilePaths = Directory.EnumerateFiles(arffDirPath, "*.arff");
            return LoadArffFiles(arffFilePaths);
        }
        public static Instances LoadArffFilesParallel(IEnumerable<string> arffFilePaths)
        {
            var bag = new ConcurrentBag<Instances>(); 
            Parallel.ForEach(arffFilePaths, arffFilePath =>
            {
                bag.Add(LoadArffFile(arffFilePath));
            });
            return ConcatenateInstances(bag);
        }

        /// <summary>
        /// Merge child instances to parent instances.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public static void ConcatenateInstances(ref Instances parent, Instances child)
        {
            for (int attrIdx = 0; attrIdx < child.numInstances(); attrIdx++)
            {
                parent.add(child.instance(attrIdx));
            }
        }
        public static void ConcatenateInstances(ref Instances parent, IEnumerable<Instances> children)
        {
            foreach (var child in children)
            {
                ConcatenateInstances(ref parent, child);
            }
        }
        public static Instances ConcatenateInstances(IEnumerable<Instances> instances)
        {
            var parent = instances.First().DeepClone(); // deep clone so that it would not overwrite arg instances
            foreach (var child in instances.Skip(1))
            {
                ConcatenateInstances(ref parent, child);
            }
            return parent;
        }

        #region Attribute related methods

        public static weka.core.Attribute CreateClassAttribute(IEnumerable<string> classValues)
        {
            var clsVector = new FastVector(classValues.Count());
            foreach (var classVal in classValues)
            {
                clsVector.addElement(classVal);
            }
            return new weka.core.Attribute("class", clsVector);
        }

        public static Tuple<weka.core.Attribute, double[]> GetAttributeValues(Instances instances, int attributeIndex)
        {
            var attr = instances.attribute(attributeIndex);
            var srcNumInsts = instances.numInstances();
            var attrVals = new double[srcNumInsts];
            for (int instIdx = 0; instIdx < srcNumInsts; instIdx++)
            {
                attrVals[instIdx] = instances.instance(instIdx).value(attributeIndex);
            }
            return new Tuple<weka.core.Attribute, double[]>(attr, attrVals);
        }
        public static Tuple<weka.core.Attribute, double[]> GetClassValues(Instances instances)
        {
            return GetAttributeValues(instances, instances.classIndex());
        }

        public static Instances CloneInstancesWithStringClass(Instances instances, IEnumerable<int> cloningIdxes = null)
        {
            if (cloningIdxes == null) cloningIdxes = Enumerable.Range(0, instances.numInstances());
            // in weka, string value is not cloned (shallow copy)
            var newInsts = new Instances(instances, cloningIdxes.Count());
            foreach (var instIdx in cloningIdxes)
            {
                var srcInst = instances.instance(instIdx);
                newInsts.add(new Instance(srcInst));
                //newInsts.instance(instIdx).setValue(instances.classIndex(), srcInst.classValue().ToString());
            }
            return newInsts;
        }

        // since reduce attributes function is slow when handling big size data or multiple attribute, create new instance
        // class idx should not be contained in remainingAttrIdxes
        public static Instances CloneInstancesByAttrs(this Instances srcInsts, IEnumerable<int> remainingAttrIdxes)
        {
            var srcAttrCount = srcInsts.numAttributes();
            var dstAttrCount = remainingAttrIdxes.Count();
            var relationVector = new FastVector(dstAttrCount);
            foreach (var attrIdx in remainingAttrIdxes)
            {
                relationVector.addElement(srcInsts.attribute(attrIdx));
            }
            relationVector.addElement(srcInsts.classAttribute()); // add class
            var dstInsts = new Instances(srcInsts.relationName(), relationVector, srcInsts.numInstances());
            dstInsts.setClassIndex(dstAttrCount);
            for (var instIdx = 0; instIdx < srcInsts.numInstances(); instIdx++)
            {
                var srcInst = srcInsts.instance(instIdx);
                var newInst = new Instance(dstAttrCount + 1); // + 1 for class
                var dstAttrIdx = 0;
                foreach (var srcAttrIdx in remainingAttrIdxes)
                {
                    newInst.setValue(dstAttrIdx++, srcInst.value(srcAttrIdx));
                }
                newInst.setValue(dstAttrCount, srcInst.classValue());
                dstInsts.add(newInst);
            }
            return dstInsts;
        }
        public static Instances CloneInstancesByAttrs(this Instances srcInsts, IEnumerable<string> attrNames, bool remainAttr = true)
        {
            var remainingIdxes = new List<int>();
            var attrHash = new HashSet<string>(attrNames);
            for (int attrIdx = 0; attrIdx < srcInsts.numAttributes(); attrIdx++)
            {
                if (attrIdx == srcInsts.classIndex()) continue;
                if (attrHash.Contains(srcInsts.attribute(attrIdx).name()) == remainAttr) remainingIdxes.Add(attrIdx);
            }
            return CloneInstancesByAttrs(srcInsts, remainingIdxes);
        }
        public static void ReduceClassAttribute(ref Instances instances)
        {
            var classAttrIndex = instances.classIndex();
            instances.setClassIndex(-1); // you cannot remove class attribute if you specify the index
            instances.deleteAttributeAt(classAttrIndex);
        }
        public static void ReduceAttributes(ref Instances instances, IEnumerable<int> removingAttrIndices)
        {
            // By removing from larger index, you can simply reduce attributes
            foreach (var removingIdx in removingAttrIndices.OrderByDescending(intVal => intVal))
            {
                instances.deleteAttributeAt(removingIdx);
            }
        }
        public static void ReduceAttributes(ref Instances instances, IEnumerable<string> removingAttrNames)
        {
            var removingAttrIndices = new List<int>();
            var removingAttrHash = new HashSet<string>(removingAttrNames);
            for (int attrIdx = 0; attrIdx < instances.numAttributes(); attrIdx++)
            {
                if (removingAttrHash.Contains(instances.attribute(attrIdx).name())) removingAttrIndices.Add(attrIdx);
            }
            ReduceAttributes(ref instances, removingAttrIndices);
        }

        // last attr of addingAttributes has to be class
        public static Instances AppendAttributes(Instances classRemovedSrcInsts, Tuple<weka.core.Attribute, double[]> classValues, IEnumerable<Tuple<weka.core.Attribute, double[]>> addingAttributes = null)
        {
            var srcAttrCount = classRemovedSrcInsts.numAttributes();
            var appendAttrCount = (addingAttributes != null) ? addingAttributes.Count() : 0;
            var dstAttrCount = srcAttrCount + appendAttrCount + 1;// + 1 for class
            var relationVector = new FastVector(dstAttrCount);
            for (int attrIdx = 0; attrIdx < srcAttrCount; attrIdx++)
            {
                relationVector.addElement(classRemovedSrcInsts.attribute(attrIdx));
            }
            if (addingAttributes != null)
                foreach (var attr in addingAttributes)
                {
                    relationVector.addElement(attr.Item1);
                }
            relationVector.addElement(classValues.Item1);

            // create new instances
            var dstInsts = new Instances(classRemovedSrcInsts.relationName(), relationVector, classRemovedSrcInsts.numInstances());
            dstInsts.setClassIndex(dstAttrCount - 1);
            for (var instIdx = 0; instIdx < classRemovedSrcInsts.numInstances(); instIdx++)
            {
                var srcInst = classRemovedSrcInsts.instance(instIdx);
                var newInst = new Instance(dstAttrCount);
                var dstAttrIdx = 0;
                for (int attrIdx = 0; attrIdx < srcAttrCount; attrIdx++)
                {
                    newInst.setValue(dstAttrIdx++, srcInst.value(attrIdx)); // copy                    
                }
                if(addingAttributes != null)
                    foreach (var addingAttr in addingAttributes)
                    {
                        newInst.setValue(dstAttrIdx++, addingAttr.Item2[instIdx]);
                    }
                newInst.setValue(dstAttrIdx, classValues.Item2[instIdx]); // add class
                dstInsts.add(newInst);
            }
            return dstInsts;
        }

        public static Instances InsertAttributes(ref Instances srcInsts, IEnumerable<Tuple<weka.core.Attribute, double[]>> addingAttributes)
        {
            var classVals = GetClassValues(srcInsts);
            ReduceClassAttribute(ref srcInsts);
            return AppendAttributes(srcInsts, classVals, addingAttributes);
        }

        /// <summary>
        /// Extract attributes which match the regular expression
        /// </summary>
        /// <param name="instances"></param>
        /// <param name="regexpStr"></param>
        /// <returns></returns>
        public static void ExtractAttributesByRegexp(ref Instances instances, string regexpStr)
        {
            var removingAttrIndices = new List<int>();
            var regexp = new Regex(regexpStr);
            for (int attrIdx = 0; attrIdx < instances.numAttributes(); attrIdx++)
            {
                if (attrIdx == instances.classIndex()) continue;
                var attributeName = instances.attribute(attrIdx).name();
                if (regexp.IsMatch(attributeName) == false) removingAttrIndices.Add(attrIdx);
            }
            ReduceAttributes(ref instances, removingAttrIndices);
        }

        
        #endregion

        #region Instance related methods


        /// <summary>
        /// Equalize the num of instance per classes. Classes have to have string or descrete values
        /// </summary>
        /// <param name="instances"></param>
        /// <param name="numInstances"></param>
        /// <returns></returns>
        public static void EqualizeInstancesNumByClass(ref Instances instances, int numInstances)
        {
            var instanceNumDict = new Dictionary<double, int>();
            var removeIndices = new List<int>();
            // By removing from larger index, you can simply reduce instances
            for (var instIdx = 0; instIdx < instances.numInstances(); instIdx++)
            {
                var classVal = instances.instance(instIdx).classValue();
                if (instanceNumDict.ContainsKey(classVal))
                {
                    if (instanceNumDict[classVal] >= numInstances)
                        removeIndices.Add(instIdx);
                    else
                        instanceNumDict[classVal]++;
                }
                else
                {
                    instanceNumDict[classVal] = 1;
                }
            }
            ReduceInstances(ref instances, removeIndices);
        }

        public static Tuple<Instances, Instances> SplitInstances(Instances instances, IEnumerable<int> instIndices)
        {
            var instIndicesHash = new HashSet<int>(instIndices);
            var insts1 = new Instances(instances, instIndicesHash.Count);
            var insts2 = new Instances(instances, instances.numInstances() - instIndicesHash.Count);
            for (int instIdx = 0; instIdx < instances.numInstances(); instIdx++)
            {
                var inst = instances.instance(instIdx);
                if (instIndicesHash.Contains(instIdx))
                    insts1.add(inst);
                else
                    insts2.add(inst);
            }
            return new Tuple<Instances, Instances>(insts1, insts2);
        }

        // instsBorderIndices is like {10,18,35,55}
        public static Instances[] SplitInstancesByBorder(Instances instances, IEnumerable<int> instsBorderIndices)
        {
            var numInsts = instances.numInstances();
            var borderInstsIdx = 0;
            var borderIdxArr = instsBorderIndices.ToArray();
            var splitInsts = new Instances[borderIdxArr.Length];
            var workingInsts = new Instances(instances, borderIdxArr[borderInstsIdx]); // first insts
            for (int instIdx = 0; instIdx < numInsts; instIdx++)
            {
                if (instIdx == borderIdxArr[borderInstsIdx])
                {
                    splitInsts[borderInstsIdx] = workingInsts;
                    borderInstsIdx++;
                    workingInsts = new Instances(instances, borderIdxArr[borderInstsIdx] - instIdx);
                }
                workingInsts.add(instances.instance(instIdx));
            }
            splitInsts[borderInstsIdx] = workingInsts; // for the last one
            return splitInsts;
        }

        public static void ReduceInstances(ref Instances instances, IEnumerable<int> instIndices, bool removeIndices = true)
        {
            if (removeIndices == false)
            {
                // create removing inst indices
                var remainIndices = new HashSet<int>(instIndices);
                instIndices = Enumerable.Range(0, instances.numInstances()).Where(idx => remainIndices.Contains(idx) == false);
            }
            // By removing from larger index, you can simply reduce instances
            foreach (var removingInstIdx in instIndices.OrderByDescending(intVal => intVal))
            {
                instances.delete(removingInstIdx);
            }
        }

        public static Instances ExtractInstancesByClass(Instances instances, string classStr)
        {
            var cloningInstIndices = new List<int>();
            for (int instIdx = 0; instIdx < instances.numInstances(); instIdx++)
            {
                var inst = instances.instance(instIdx);
                // If class value of one instance is not specified class, remove it
                if (inst.stringValue(inst.numAttributes() - 1) == classStr) cloningInstIndices.Add(instIdx);
            }
            return CloneInstancesWithStringClass(instances, cloningInstIndices);
        }

        public static void RandomSampleInstances(ref Instances instances, double sampleRate = 0.2)
        {
            if (sampleRate <= 0 || sampleRate >= 1) throw new ArgumentOutOfRangeException("sampleRate");
            var rnd = new Random();
            var instNum = instances.numInstances();
            var sampleNum = (int)(instNum * sampleRate);
            var removingInstIndices = Enumerable.Range(0, instNum).ToList();
            for (int i = 0; i < sampleNum; i++)
            {
                var selectedIdx = (int)(rnd.NextDouble() * (removingInstIndices.Count - 1));
                removingInstIndices.RemoveAt(selectedIdx); // Ignore indices which are selected
            }
            ReduceInstances(ref instances, removingInstIndices);
        }

        
        #endregion


        public static Instances NormalizeInstances(Instances instances, double scale = 1, double offset = 0, string normInfoPath = null)
        {
            var normFilter = new Normalize();
            normFilter.setInputFormat(instances);
            normFilter.setScale(scale);
            normFilter.setTranslation(offset);
            var normInsts = Filter.useFilter(instances, normFilter);
            if (normInfoPath != null)
            {
                General.ExportJavaObject(normInfoPath, normFilter);
            }
            return normInsts;
        }
        public static Instances NormalizeInstances(string normInfoPath, Instances instances)
        {
            if (File.Exists(normInfoPath) == false) throw new ArgumentException("Have to specify correct normInfoPath");
            var normFilter = General.ImportJavaObject(normInfoPath) as Normalize;
            var normInsts = Filter.useFilter(instances, normFilter);
            return normInsts;
        }

        public static Instances[] NormalizeInstances(IEnumerable<Instances> instances, double scale = 1, double offset = 0)
        {
            var mergedInsts = ConcatenateInstances(instances);
            int instBorder = 0;
            var instsBorderIdxs = instances.Select(insts => { instBorder += insts.numInstances(); return instBorder; });
            mergedInsts = NormalizeInstances(mergedInsts, scale, offset);
            return SplitInstancesByBorder(mergedInsts, instsBorderIdxs);
        }

        /// <summary>
        /// Save instances as a ARFF to specified path
        /// </summary>
        /// <param name="instances"></param>
        /// <param name="savePath"></param>
        public static void SaveToArff(Instances instances, string savePath)
        {
            var parentPath = Path.GetDirectoryName(savePath);
            if (Directory.Exists(parentPath) == false) Directory.CreateDirectory(parentPath);
            var arffSaver = new ArffSaver();
            arffSaver.setInstances(instances);
            arffSaver.setFile(new java.io.File(savePath));
            arffSaver.writeBatch();
        }

        public static weka.core.Instances MakeInstancesFromArr(IEnumerable<double[]> dataVals, string relationName = "relation", IEnumerable<string> attributes = null)
        {
            var dimNum = dataVals.First().Length;
            var dataAttrs = new weka.core.FastVector(dimNum);
            if (attributes == null)
            {
                attributes = Enumerable.Range(0, dimNum).Select(i => "attr_" + i);
            }
            foreach (var attr in attributes) dataAttrs.addElement(new weka.core.Attribute(attr));

            var instances = new weka.core.Instances(relationName, dataAttrs, dataVals.Count());
            foreach (var vec in dataVals)
            {
                var inst = new weka.core.Instance(dimNum);
                for (int i = 0; i < dimNum; i++)
                {
                    inst.setValue((weka.core.Attribute)(dataAttrs.elementAt(i)), vec[i]);
                }
                instances.add(inst);
            }
            return instances;
        }

    }
}
