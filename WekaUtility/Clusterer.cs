﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using weka.clusterers;
using weka.core;

namespace WekaUtility
{
    public static class Clusterer
    {
        public static weka.clusterers.Clusterer InitClusterer(WekaConstantParams.Clusterers clustererEnum, int clusterNum = 4, string optionStr = null)
        {
            weka.clusterers.Clusterer clusterer = null;
            switch (clustererEnum)
            {
                case WekaConstantParams.Clusterers.KMeans: clusterer = new weka.clusterers.SimpleKMeans(); break;
                case WekaConstantParams.Clusterers.XMeans: clusterer = new weka.clusterers.XMeans(); break;
                case WekaConstantParams.Clusterers.EM: clusterer = new weka.clusterers.EM(); break;
                case WekaConstantParams.Clusterers.Hierarchical: clusterer = new weka.clusterers.HierarchicalClusterer(); break;
                default: throw new Exception("Not supported Clusterer!");
            }
            if (clusterer != null)
            {
                SetOptions(ref clusterer, clustererEnum, clusterNum, optionStr);
            }
            return clusterer;
        }

        public static void SetOptions(ref weka.clusterers.Clusterer clusterer, WekaConstantParams.Clusterers clustererEnum, int clusterNum = 4, string optionStr = null)
        {
            switch (clustererEnum)
            {
                case WekaConstantParams.Clusterers.KMeans:
                    if(optionStr == null)
                        optionStr = "-N " + clusterNum + " -A \"weka.core.EuclideanDistance -R first-last\" -I 500 -S 10";
                    ((SimpleKMeans)clusterer).setOptions(weka.core.Utils.splitOptions(optionStr));
                    break;
                case WekaConstantParams.Clusterers.XMeans:
                    if (optionStr == null)
                        optionStr = "-I 1 -M 1000 -J 1000 -L 4 -H 4 -B 1.0 -C 0.5 -D \"weka.core.EuclideanDistance -R first-last\" -S 10";
                    ((XMeans)clusterer).setOptions(weka.core.Utils.splitOptions(optionStr));
                    break;
                case WekaConstantParams.Clusterers.EM:
                    if (optionStr == null)
                        optionStr = "-I 100 -N " + clusterNum + " -M 1.0E-6 -S 100";
                    ((EM)clusterer).setOptions(weka.core.Utils.splitOptions(optionStr));
                    break;
                case WekaConstantParams.Clusterers.Hierarchical:
                    if (optionStr == null)
                        optionStr = "-N " + clusterNum + " -L SINGLE -P -A \"weka.core.EuclideanDistance -R first-last\"";
                    ((HierarchicalClusterer)clusterer).setOptions(weka.core.Utils.splitOptions(optionStr));
                    break;
                default: throw new Exception("Not supported Clusterer!");
            }
        }

        public static void Train(ref weka.clusterers.Clusterer clusterer, Instances trainingInsts, string modelSavePath = null)
        {
            if (clusterer == null) throw new ArgumentNullException();
            trainingInsts.setClassIndex(trainingInsts.numAttributes() - 1);
            clusterer.buildClusterer(trainingInsts);
            if (modelSavePath != null) General.ExportJavaObject(modelSavePath, clusterer);
        }

        public static weka.clusterers.Clusterer Load(string modelFilePath, WekaConstantParams.Clusterers clustererEnum, string optionStr = null)
        {
            var clusterer = (weka.clusterers.Clusterer)General.ImportJavaObject(modelFilePath);
            if (clusterer != null && optionStr != null) SetOptions(ref clusterer, clustererEnum, optionStr: optionStr);
            return clusterer;
        }

        public static string GetDendrogram(weka.clusterers.Clusterer clusterer)
        {
            var hc = clusterer as HierarchicalClusterer;
            if (hc == null) throw new Exception("Only HierarchicalClusterer supports dendrogram string");
            return hc.graph();
        }
    }
}
