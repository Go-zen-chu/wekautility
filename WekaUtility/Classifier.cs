﻿using java.io;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using weka.classifiers;
using weka.classifiers.bayes;
using weka.classifiers.functions;
using weka.classifiers.lazy;
using weka.classifiers.meta;
using weka.classifiers.trees;
using weka.core;
using WekaUtility.Extern;

namespace WekaUtility
{
    public static class Classifier
    {
        public static weka.classifiers.Classifier InitClassifier(WekaConstantParams.Classifiers classifierEnum, string optionStr = null)
        {
            weka.classifiers.Classifier classifier = null;
            switch (classifierEnum)
            {
                case WekaConstantParams.Classifiers.J48: classifier = new J48(); break;
                case WekaConstantParams.Classifiers.SMO: classifier = new SMO(); break;
                case WekaConstantParams.Classifiers.DistanceSMO: classifier = new DistanceSMO(); break;
                case WekaConstantParams.Classifiers.NaiveBayes: classifier = new NaiveBayes(); break;
                case WekaConstantParams.Classifiers.AdaBoost: classifier = new AdaBoostM1(); break;
                case WekaConstantParams.Classifiers.Logistic: classifier = new Logistic(); break;
                case WekaConstantParams.Classifiers.J48graft: classifier = new J48graft(); break;
                case WekaConstantParams.Classifiers.LADTree: classifier = new LADTree(); break;
                case WekaConstantParams.Classifiers.libSVM: classifier = new LibSVM(); break;
                case WekaConstantParams.Classifiers.SMOreg: classifier = new SMOreg(); break;
                case WekaConstantParams.Classifiers.LinearRegression: classifier = new LinearRegression(); break;
                case WekaConstantParams.Classifiers.MultilayerPerception: classifier = new MultilayerPerceptron(); break;
                case WekaConstantParams.Classifiers.LeastMedSq: classifier = new LeastMedSq(); break;
                case WekaConstantParams.Classifiers.DecisionStump: classifier = new DecisionStump(); break;
                case WekaConstantParams.Classifiers.M5P: classifier = new M5P(); break;
                case WekaConstantParams.Classifiers.RandomForest: classifier = new RandomForest(); break;
                case WekaConstantParams.Classifiers.IBk: classifier = new IBk(); break;
                case WekaConstantParams.Classifiers.MKL: classifier = new MultipleKernelLearning(); break;
                default: throw new Exception("Not supported Classifier!");
            }
            if (classifier != null)
            {
                SetOptions(ref classifier, classifierEnum, optionStr);
            }
            return classifier;
        }

        public static void SetOptions(ref weka.classifiers.Classifier classifier, WekaConstantParams.Classifiers classifierEnum, string optionStr = null)
        {
            if (optionStr != null)
            {
                classifier.setOptions(weka.core.Utils.splitOptions(optionStr));
                return;
            }
            // Set default settings
            switch (classifierEnum)
            {
                case WekaConstantParams.Classifiers.J48:
                    classifier.setOptions(new string[]{"-C", "0.25","-M","2"});
                    break;
                case WekaConstantParams.Classifiers.SMO:
                    classifier.setOptions(weka.core.Utils.splitOptions("-C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0\""));
                    break;
                case WekaConstantParams.Classifiers.AdaBoost:
                    classifier.setOptions(new string[]{"-P","100","-S","1","-I","10","-W","weka.classifiers.trees.DecisionStump"});
                    break;
                case WekaConstantParams.Classifiers.Logistic:
                    classifier.setOptions(new string[] {"-R","1.0E-8","-M","-1"});
                    break;
                case WekaConstantParams.Classifiers.J48graft:
                    classifier.setOptions(new string[] {"-C","0.1","-M","-10"});
                    break;
                case WekaConstantParams.Classifiers.LADTree:
                    classifier.setOptions(new string[] {"-B","10" });
                    break;
                case WekaConstantParams.Classifiers.libSVM:
                    {
                        String[] options = new String[20];
                        options[0] = "-S";
                        options[1] = "0";
                        options[2] = "-K";
                        options[3] = "2";
                        options[4] = "-D";
                        options[5] = "3";
                        options[6] = "-G";
                        options[7] = "0.0";
                        options[8] = "-R";
                        options[9] = "0.0";
                        options[10] = "-N";
                        options[11] = "0.5";
                        options[12] = "-M";
                        options[13] = "40.0";
                        options[14] = "-C";
                        options[15] = "1.0";
                        options[16] = "-E";
                        options[17] = "0.0010";
                        options[18] = "-P";
                        options[19] = "0.1";
                        classifier.setOptions(options);
                        break;
                    }
                case WekaConstantParams.Classifiers.SMOreg:
                    {
                        String[] options = new String[14];
                        options[0] = "-S";
                        options[1] = "0.0010";
                        options[2] = "-C";
                        options[3] = "1.0";
                        options[4] = "-G";
                        options[5] = "0.01";
                        options[6] = "-A";
                        options[7] = "250007";
                        options[8] = "-T";
                        options[9] = "0.0010";
                        options[10] = "-P";
                        options[11] = "1.0E-12";
                        options[12] = "-N";
                        options[13] = "0";
                        classifier.setOptions(options);
                        break;
                    }
                case WekaConstantParams.Classifiers.LinearRegression:
                    classifier.setOptions(new string[] { "-S", "0", "-R", "1.0E-8" });
                    break;
                case WekaConstantParams.Classifiers.MultilayerPerception:
                    {
                        String[] options = new String[14];
                        options[0] = "-L";
                        options[1] = "0.3";
                        options[2] = "-M";
                        options[3] = "0.2";
                        options[4] = "-N";
                        options[5] = "500";
                        options[6] = "-V";
                        options[7] = "0";
                        options[8] = "-S";
                        options[9] = "0";
                        options[10] = "-E";
                        options[11] = "20";
                        options[12] = "-H";
                        options[13] = "a";
                        classifier.setOptions(options);
                        break;
                    }
                case WekaConstantParams.Classifiers.LeastMedSq:
                    classifier.setOptions(new string[] { "-S", "4", "-G", "0" });
                    break;
                case WekaConstantParams.Classifiers.IBk:
                    classifier.setOptions(new string[] { "-K", "3" });
                    break;
                case WekaConstantParams.Classifiers.RandomForest:
                    classifier.setOptions(new string[] { "-I", "3", "-depth", "20" });
                    break;
            }
        }

        public static void Train(ref weka.classifiers.Classifier classifier, Instances trainingInsts, string modelSavePath = null)
        {
            if (classifier == null) throw new ArgumentNullException();
            trainingInsts.setClassIndex(trainingInsts.numAttributes() - 1);
            classifier.buildClassifier(trainingInsts);
            if (modelSavePath != null) General.ExportJavaObject(modelSavePath, classifier);
        }

        public static weka.classifiers.Classifier Load(string modelFilePath, WekaConstantParams.Classifiers classifierEnum, string optionStr = null)
        {
            var classifier = (weka.classifiers.Classifier)General.ImportJavaObject(modelFilePath);
            if (classifier != null && optionStr != null) SetOptions(ref classifier, classifierEnum, optionStr);
            return classifier;
        }

        /// <summary>
        /// Classify instance and returns the possibilities which the instance belongs to the class
        /// </summary>
        /// <param name="classifier"></param>
        /// <param name="inst"></param>
        /// <returns></returns>
        public static double[] Classify(weka.classifiers.Classifier classifier, Instance inst)
        {
            return classifier.distributionForInstance(inst);
        }

        public static Evaluation Evaluate(weka.classifiers.Classifier classifier, Instances testInstances)
        {
            testInstances.setClassIndex(testInstances.numAttributes() - 1);
            var eval = new Evaluation(testInstances);
            eval.useNoPriors();
            eval.evaluateModel(classifier, testInstances);
            return eval;
        }

        public static double[][] GetConfusionMatrix(weka.classifiers.Classifier classifier, Instances testInstances)
        {
            var eval = Evaluate(classifier, testInstances);
            return eval.confusionMatrix();
        }

        public static string GetEvaluationStringAsync(weka.classifiers.Classifier classifier, Instances testInstances)
        {
            var eval = Evaluate(classifier, testInstances);
            return new StringBuilder(eval.toSummaryString()).Append(System.Environment.NewLine)
                                .Append(eval.toMatrixString()).Append(System.Environment.NewLine)
                                .Append(eval.toClassDetailsString()).ToString();
        }

        public static void ExportEvaluation(weka.classifiers.Classifier classifier, Instances testInstances, string testResultPath)
        {
            var evalStr = GetEvaluationStringAsync(classifier, testInstances);
            using (var sw = new StreamWriter(testResultPath))
            {
                sw.WriteLine(evalStr);
            }
        }

    }
}
