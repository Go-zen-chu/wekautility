﻿using java.io;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using weka.filters.unsupervised.attribute;

namespace WekaUtility
{
    public static class General
    {
        public static void ExportJavaObject(string exportFilePath, object obj)
        {
            using (var oos = new ObjectOutputStream(new FileOutputStream(exportFilePath)))
            {
                oos.writeObject(obj);
                oos.flush();
            }
        }

        public static object ImportJavaObject(string importFilePath)
        {
            using (var ois = new ObjectInputStream(new FileInputStream(importFilePath)))
            {
               return ois.readObject();
            }
        }

        public static Tuple<bool, string> ExecuteCommand(string programName, string argmuments, int timeOutMillisec = int.MaxValue)
        {
            var isTimeOut = false;
            var queue = new ConcurrentQueue<string>(); // output data received is called asyncnously you need to lock or use concurrent data storage
            using (var p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardInput = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.Arguments = argmuments;
                p.StartInfo.FileName = programName;
                p.Start();

                p.OutputDataReceived += (sender, e) => { if (e.Data != null) queue.Enqueue(e.Data); };
                p.BeginOutputReadLine();
                //string results = p.StandardOutput.ReadToEnd(); // this will hang if external program keeps using std out
                if (timeOutMillisec == int.MaxValue)
                {
                    p.WaitForExit(); // wait infinity
                }
                else
                {
                    if (p.WaitForExit(timeOutMillisec) == false)
                    {
                        isTimeOut = true;
                        p.CancelOutputRead();
                        p.Kill();
                    }
                }
            }
            return new Tuple<bool, string>(isTimeOut, string.Join(Environment.NewLine, queue));
        }

        public static string DoubleQuote(this string input)
        {
            return new StringBuilder().Append("\"").Append(input).Append("\"").ToString();
        }
    }
}
