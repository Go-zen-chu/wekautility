﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WekaUtility
{
    public static class WekaConstantParams
    {
        public enum Classifiers
        {
            /// <summary>
            /// C4.5決定木
            /// </summary>
            J48,
            /// <summary>
            /// SMO(SVM)
            /// </summary>
            SMO,
            /// <summary>
            /// 分類面からの距離を求められるSMO(SVM)
            /// </summary>
            DistanceSMO,
            /// <summary>
            /// ナイーブベイズ
            /// </summary>
            NaiveBayes,
            /// <summary>
            /// ロジスティック回帰
            /// </summary>
            Logistic,
            /// <summary>
            /// AdaBoost
            /// </summary>
            AdaBoost,
            /// <summary>
            /// Grafted C4.5決定木
            /// </summary>
            J48graft,
            /// <summary>
            /// libSVMによるSVM
            /// </summary>
            libSVM,
            /// <summary>
            /// LogitBoostによる決定木
            /// </summary>
            LADTree,
            RandomForest,
            /// <summary>
            /// モデル木であるM5P
            /// </summary>
            M5P,
            DecisionStump,
            SMOreg,
            LinearRegression,
            LeastMedSq,
            MultilayerPerception,
            IBk,
            MKL
        };
        /// <summary>
        /// クラスタ分析　KMeans，XMeans，EM
        /// </summary>
        public enum Clusterers
        {
            /// <summary>
            /// K平均法（K-means）
            /// </summary>
            KMeans,
            /// <summary>
            /// X-means
            /// </summary>
            XMeans,
            /// <summary>
            /// EMアルゴリズムによるクラスタリング
            /// </summary>
            EM,
            Hierarchical
        };
        /// <summary>
        /// Wekaフィルタ　PrincipalComponents，RandomProjection
        /// </summary>
        public enum Filters
        {
            /// <summary>
            /// 主成分分析（PCA）
            /// </summary>
            PrincipalComponents,
            /// <summary>
            /// ランダムプロジェクション
            /// </summary>
            RandomProjection
        };
        /// <summary>
        /// 特徴選択用の評価器
        /// </summary>
        public enum FeatureEvaluators
        {
            /// <summary>
            /// 情報利得
            /// </summary>
            InformationGain,
            /// <summary>
            /// ReliefF
            /// </summary>
            ReliefF
        };

        public static Dictionary<string, WekaConstantParams.Classifiers> GetClassifiersDict()
        {
            var classifiersDict = new Dictionary<string, WekaConstantParams.Classifiers>();
            foreach (var classifier in Enum.GetValues(typeof(WekaConstantParams.Classifiers)) as WekaConstantParams.Classifiers[])
            {
                classifiersDict.Add(classifier.ToString(), classifier);
            }
            return classifiersDict;
        }

    }
}
