﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using weka.core;

namespace WekaUtility.Extern
{
    public class MultipleKernelLearning : AbstractClassifier
    {
        public enum SVMType {  C_SVC = 0, epsilon_SVR = 3 }
        public enum Regularizer { Entropic = 0, L1 = 1, L2 = 2, Lp = 3 }
        public enum Solver { SMO = 0, ReducedGradient = 1, MirrorDecent = 2 }
        public enum Constraint { Simplex = 0, NonNegativeOrthant = 1 }

        public struct KernelSetting
        {
            // -d degree : set degree in kernel function (default 3)
            public int m_degree;
            //-g gamma : set gamma in kernel function (default 1/num_features)
            public double m_gamma;
            //-r coef0 : set coef0 in kernel function (default 0)
            public double m_coef0;
            //-w weight: set weight of the kernel (default 1/num_kernels) 
            public double m_weight;
        }

        // -a : number of base kernels (mandatory option)
        static internal int m_numKernels = 3;
        // -s svm_type : set type of SVM (default 0)
        static internal SVMType m_svmType = SVMType.C_SVC;
        // -g  regularizer for d: type of regularizer to use for d (kernel weights)
        static internal Regularizer m_regularizer = Regularizer.Entropic;
        // -o : value of p for Lp regularization (default 2.0)
        static internal double m_LpRegularizerVal = 2.0;
        // -f  solver : type of solver to use for d (kernel weights)
        static internal Solver m_solver = Solver.ReducedGradient;
        // -j  constraint : type of constraints to use for d (kernel weights)
        static internal Constraint m_constraint = Constraint.Simplex;
        /* -k kernel_file : specify the file containing kernel information.
         * It should have kernels greater than or equal to the number specified by option "-a".
         * First "-a" kernels are choosen if more than "-a" kernels are specified in kernel_file.
         * Each line in kernel_file should be in the one of the following two formats:
           -t 4 -f kernel_values_filename (precomputed kernel only), OR -t kernel_type -d degree -g gamma -r coef0 (kernel computed on the fly)
           The valid kernel_type are
	           0 -- linear: u'*v
	           1 -- polynomial: (gamma*u'*v + coef0)^degree
	           2 -- radial basis function: exp(-gamma*|u-v|^2)
	           3 -- sigmoid: tanh(gamma*u'*v + coef0)
	           4 -- precomputed kernel (kernel values in the specified file)
            -d degree : set degree in kernel function (default 3)
            -g gamma : set gamma in kernel function (default 1/num_features)
            -r coef0 : set coef0 in kernel function (default 0)
            -w weight: set weight of the kernel (default 1/num_kernels) 
         */
        static internal string m_kernel_path = null;
        // -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
        static internal double m_cost = 1.0;
        // -l lambda : set the tuning parameter that trades off between regularizer and the objective function (default 0.1)
        static internal double m_lambda = 0.1;
        // -y obj_threshold : set the threshold for the decrease of objective function in line search 
        //static internal double m_obj_threshold = 0;
        // -z diff_threshold : set the threshold that affects when line search terminates
        //static internal double m_diff_threshold = 0;
        // -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
        static internal double m_nu = 0.5;
        // -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
        static internal double m_epsilonSVR = 0.1;
        // -m cachesize : set cache memory size per kernel in MB (default 100)
        static internal int m_cachesize = 100;
        // -e epsilon : set tolerance of termination criterion (default 0.001)
        static internal double m_epsilon = 0.001;
        // -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
        static internal int m_shrinking = 1;
        // -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
        static internal int m_probability_estimates = 0;
        // -wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
        static internal double m_weightSVC = 1.0;

        // custom options
        // path for saving training instances 
        string m_trainingDataExportPath = null;
        string m_modelExportPath = null;
        string m_testDataPath = null;
        string m_resultPath = null;

        public bool BuiltSuccessful { get; set; }

        Dictionary<weka.core.Instance, double[]> distributionResultDict = new Dictionary<Instance, double[]>();
        Dictionary<weka.core.Instance, double> classificationResultDict = new Dictionary<Instance, double>();

        public override void setOptions(string[] options)
        {
            string tmpStr;
            // custom option
            tmpStr = Utils.getOption("train", options);
            if (tmpStr.Length != 0) m_trainingDataExportPath = tmpStr;

            tmpStr = Utils.getOption("model", options);
            if (tmpStr.Length == 0)
                throw new ArgumentException("train and model parameters are mandatory");
            else
                m_modelExportPath = tmpStr;

            tmpStr = Utils.getOption("a", options);
            if(tmpStr.Length != 0) m_numKernels = int.Parse(tmpStr);
            tmpStr = Utils.getOption("s", options);
            if (tmpStr.Length != 0) m_svmType = (SVMType)int.Parse(tmpStr);
            tmpStr = Utils.getOption("g", options);
            if (tmpStr.Length != 0) m_regularizer = (Regularizer)int.Parse(tmpStr);
            tmpStr = Utils.getOption("o", options);
            if (tmpStr.Length != 0) m_LpRegularizerVal = double.Parse(tmpStr);
            tmpStr = Utils.getOption("f", options);
            if (tmpStr.Length != 0) m_solver = (Solver)int.Parse(tmpStr);
            tmpStr = Utils.getOption("j", options);
            if (tmpStr.Length != 0) m_constraint = (Constraint)int.Parse(tmpStr);
            
            tmpStr = Utils.getOption("k", options);
            if (tmpStr.Length != 0) m_kernel_path = tmpStr;

            tmpStr = Utils.getOption("c", options);
            if (tmpStr.Length != 0) m_cost = double.Parse(tmpStr);
            tmpStr = Utils.getOption("l", options);
            if (tmpStr.Length != 0) m_lambda = double.Parse(tmpStr);
            tmpStr = Utils.getOption("n", options);
            if (tmpStr.Length != 0) m_nu = double.Parse(tmpStr);
            tmpStr = Utils.getOption("p", options);
            if (tmpStr.Length != 0) m_epsilonSVR = double.Parse(tmpStr);
            tmpStr = Utils.getOption("m", options);
            if (tmpStr.Length != 0) m_cachesize = int.Parse(tmpStr);            
            tmpStr = Utils.getOption("e", options);
            if (tmpStr.Length != 0) m_epsilon = double.Parse(tmpStr);            
            tmpStr = Utils.getOption("h", options);
            if (tmpStr.Length != 0) m_shrinking = int.Parse(tmpStr);
            tmpStr = Utils.getOption("b", options);
            if (tmpStr.Length != 0) m_probability_estimates = int.Parse(tmpStr);
            tmpStr = Utils.getOption("wi", options);
            if (tmpStr.Length != 0) m_weightSVC = double.Parse(tmpStr);


            base.setOptions(options);
        }

        List<string> getOptionList()
        {
            var optionList = new List<string>();
            optionList.Add("-a");
            optionList.Add(m_numKernels.ToString());
            optionList.Add("-s");
            optionList.Add(((int)m_svmType).ToString());
            optionList.Add("-g");
            optionList.Add(((int)m_regularizer).ToString());
            if (m_regularizer == Regularizer.Lp)
            {
                optionList.Add("-o");
                optionList.Add(m_LpRegularizerVal.ToString());
            }
            optionList.Add("-f");
            optionList.Add(((int)m_solver).ToString());
            optionList.Add("-j");
            optionList.Add(((int)m_constraint).ToString());

            optionList.Add("-k");
            optionList.Add(m_kernel_path.DoubleQuote());

            optionList.Add("-c");
            optionList.Add(m_cost.ToString());
            optionList.Add("-l");
            optionList.Add(m_lambda.ToString());
            optionList.Add("-n");
            optionList.Add(m_nu.ToString());
            optionList.Add("-p");
            optionList.Add(m_epsilonSVR.ToString());
            optionList.Add("-m");
            optionList.Add(m_cachesize.ToString());
            optionList.Add("-e");
            optionList.Add(m_epsilon.ToString());
            optionList.Add("-h");
            optionList.Add(m_shrinking.ToString());
            optionList.Add("-b");
            optionList.Add(m_probability_estimates.ToString());
            if (m_svmType == SVMType.C_SVC)
            {
                optionList.Add("-wi");
                optionList.Add(m_weightSVC.ToString());
            }
            return optionList;
        }

        public override string[] getOptions()
        {
            return getOptionList().ToArray();
        }

        public override void buildClassifier(weka.core.Instances instances)
        {
            if (m_trainingDataExportPath == null) throw new Exception("Training data path is mandatory for this function");
            // convert instances to mkl format
            using (var sw = new StreamWriter(m_trainingDataExportPath))
            {
                sw.Write(ConvertInstancesToMKLFormat(instances));
            }

            buildClassifier();
        }
        public void buildClassifier(int timeOutMillisec = int.MaxValue) //ms
        {
            if (m_trainingDataExportPath == null || File.Exists(m_trainingDataExportPath) == false || m_modelExportPath == null)
                throw new FileNotFoundException("Training data have to be exported before using this function");
            var optionList = getOptionList();
            optionList.Add(m_trainingDataExportPath.DoubleQuote());
            optionList.Add(m_modelExportPath.DoubleQuote());
            var args = string.Join(" ", optionList);
            var resultTpl = General.ExecuteCommand("svm-train.exe", args, timeOutMillisec);

            BuiltSuccessful = resultTpl.Item1 == false; // is time out
        }

        public List<double[]> classify(string testDataPath, string resultPath, bool overwriteResult = false)
        {
            if (testDataPath == null || File.Exists(testDataPath) == false)
                throw new FileNotFoundException("Test data have to be exported before using this function");

            m_testDataPath = testDataPath;
            m_resultPath = resultPath;
            
            if (overwriteResult || File.Exists(resultPath) == false)
            {
                Tuple<bool, string> resultTpl = null;
                var args = string.Join(" ", "-b", m_probability_estimates.ToString(), m_testDataPath.DoubleQuote(), m_modelExportPath.DoubleQuote(), m_resultPath.DoubleQuote());
                do
                {
                    resultTpl = General.ExecuteCommand("svm-predict.exe", args);
                } while (resultTpl.Item2 == null);
            }

            var resultData = new List<double[]>();
            var clsNum = -1;

            using (var sr = new StreamReader(resultPath))
            {
                int rowIdx = 0;
                do
                {
                    var line = sr.ReadLine().Split(' ');
                    if (rowIdx == 0)
                    {
                        clsNum = line.Length - 1; // example of the first line "labels 0 1"
                    }
                    else
                    {
                        if (m_probability_estimates == 0) // just stores classification results
                            resultData.Add(new[] { double.Parse(line[0]) });
                        else // storing distribution
                            resultData.Add(line.Select(strVal => double.Parse(strVal)).ToArray()); // careful that the first value is classification result
                    }
                    rowIdx++;
                } while (sr.Peek() != -1);
            }
            return resultData;
        }
        public List<double[]> classify(string testDataPath, Instances testInstances, string resultPath, bool overwriteResult = false)
        {
            using (var sw = new StreamWriter(testDataPath))
            {
                sw.Write(ConvertInstancesToMKLFormat(testInstances));
            }
            return classify(testDataPath, resultPath, overwriteResult);
        }

        public void setupForClassification(string testDataPath, Instances testInstances, string resultPath, bool overwriteResult = false)
        {
            var classificationResults = classify(testDataPath, testInstances, resultPath, overwriteResult);
            classificationResultDict.Clear();
            distributionResultDict.Clear();
            for (int instIdx = 0; instIdx < testInstances.numInstances(); instIdx++)
            {
                var inst = testInstances.instance(instIdx);
                // the first column value is always class idx
                classificationResultDict.Add(inst, classificationResults[instIdx][0]);
                // distribution is exported after class idx so skip first column
                if (m_probability_estimates == 1)
                    distributionResultDict.Add(inst, classificationResults[instIdx].Skip(1).ToArray());
            }
        }
        public override double[] distributionForInstance(weka.core.Instance instance)
        {
            if (distributionResultDict.Count == 0) throw new Exception("You need to turn on -b flag to calculate distribution");
            return distributionResultDict[instance];
        }
        public override double classifyInstance(weka.core.Instance instance)
        {
            if (classificationResultDict.Count == 0) throw new Exception("Needs to use setupForClassification function before classification!");
            return classificationResultDict[instance];
        }

        public static string ConvertInstancesToMKLFormat(Instances instances)
        {
            var numInsts = instances.numInstances();
            var numAttrs = instances.numAttributes();
            var mklFormatBuilder = new StringBuilder();
            for (int instIdx = 0; instIdx < numInsts; instIdx++)
			{
                var inst = instances.instance(instIdx);
                mklFormatBuilder.Append(inst.classValue()); // class value can be any integer
			    for (int attrIdx = 0; attrIdx < numAttrs; attrIdx++)
			    {
                    if (attrIdx == inst.classIndex()) continue;
                    var value = inst.value(attrIdx);
                    if (inst.isMissing(attrIdx) || double.IsNaN(value)) continue; // ignore missing
			        mklFormatBuilder.Append(" ").Append(attrIdx + 1).Append(":").Append(value); // attrIdx + 1 because the format only supports from 1
			    }
                mklFormatBuilder.AppendLine();
			}
            return mklFormatBuilder.ToString();
        }
        public static void ExportAsMKLFormat(string exportPath, Instances instances)
        {
            using (var sw = new StreamWriter(exportPath)) sw.Write(ConvertInstancesToMKLFormat(instances));
        }
    }
}
