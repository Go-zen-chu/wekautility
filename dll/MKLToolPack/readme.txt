svm-train [options] training_set_file [model_file]
svm-predict [options] test_file model_file output_file

'svm-train' Usage
=================

Usage: svm-train [options] training_set_file [model_file]

Example:
command : svm-train -s 0 -h 0 -m 400 -o 2.0 -c 10.0 -l 1.0 -f 0 -j 1 -g 3 -b 1 -a 2 -e 0.1 -k KernelFilePath MKL_TrainFilePath ModelFileSavePath;

options:
-a number of base kernels (mandatory option)
-s svm_type : set type of SVM (default 0)
	0 -- C-SVC (default)
	3 -- epsilon-SVR
-g  regularizer for d: type of regularizer to use for d (kernel weights)
    The valid regularizers are
    0 --- Entropic regularization (default)
    1 --- L1 regularization 
    2 --- L2 regularization
    3 --- Lp regularization     
-o  value of p for Lp regularization (default 2.0)
-f  solver : type of solver to use for d (kernel weights)
    The valid choices are
    0 --- SMO based (currently compatible only with entropic regularization)
    1 --- Reduced gradient based (aka SimpleMKL) (default)
    2 --- Mirror Descent (aka VSKL)
-j  constraint : type of constraints to use for d (kernel weights)
    The valid constraints are
    0 --- simplex (default)
    1 --- non-negative orthant (i.e. positive kernel weights)
-k kernel_file : specify the file containing kernel information. It should have kernels greater than or equal to the number specified by option "-a". First "-a" kernels are choosen if more than "-a" kernels are specified in kernel_file
   Each line in kernel_file should be in the one of the following two formats:
   -t 4 -f kernel_values_filename (precomputed kernel only), OR
   -t kernel_type -d degree -g gamma -r coef0 (kernel computed on the fly)
   The valid kernel_type are
	   0 -- linear: u'*v
	   1 -- polynomial: (gamma*u'*v + coef0)^degree
	   2 -- radial basis function: exp(-gamma*|u-v|^2)
	   3 -- sigmoid: tanh(gamma*u'*v + coef0)
	   4 -- precomputed kernel (kernel values in the specified file)
   -d degree : set degree in kernel function (default 3)
   -g gamma : set gamma in kernel function (default 1/num_features)
   -r coef0 : set coef0 in kernel function (default 0)
   -w weight: set weight of the kernel (default 1/num_kernels) 
-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
-l lambda : set the tuning parameter that trades off between regularizer and the objective function (default 0.1)
-y obj_threshold : set the threshold for the decrease of objective function in line search
-z diff_threshold : set the threshold that affects when line search terminates
-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
-m cachesize : set cache memory size per kernel in MB (default 100)
-e epsilon : set tolerance of termination criterion (default 0.001)
-h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
-b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
-v n: n-fold cross validation mode
-q : quiet mode (no outputs)

'svm-predict' Usage
===================

Usage: svm-predict [options] test_file model_file output_file

Example:
command : svm-predict -b 1 MKL_TestFilePath ModelFilePath ClassificationResultPath;

options:
-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); for one-class SVM only 0 is supported

'svm-scale' Usage
=================

Usage: svm-scale [options] data_filename

options:
-l lower : x scaling lower limit (default -1)
-u upper : x scaling upper limit (default +1)
-y y_lower y_upper : y scaling limits (default: no y scaling)
-s save_filename : save scaling parameters to save_filename
-r restore_filename : restore scaling parameters from restore_filename
