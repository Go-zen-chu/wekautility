﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WekaUtility;
using System.IO;
using weka.core;

namespace WekaUtilityTest
{
    [TestClass]
    public class ArffUtility_Test
    {
        public Instances GetSampleInstances()
        {
            var projectPath = System.AppDomain.CurrentDomain.BaseDirectory;
            while (Path.GetFileName(projectPath) != "WekaUtilityTest")
                projectPath = Path.GetDirectoryName(projectPath);
            var sampleDataPath = Path.Combine(projectPath, "TestData", "sampleData.arff");
            var sampleInsts = ArffUtility.LoadArffFile(sampleDataPath);
            return sampleInsts;
        }

        [TestMethod]
        public void LoadArffDataTest()
        {
            var sampleInsts = GetSampleInstances();
            if (sampleInsts != null)
            {
                Assert.AreEqual(sampleInsts.numInstances(), 7);
                Assert.AreEqual(sampleInsts.numAttributes(), 5);
            }
        }

        [TestMethod]
        public void DeepCloneTest()
        {
            var sampleInsts = GetSampleInstances();
            var cloneInsts = ArffUtility.DeepClone(sampleInsts);
            for (int instIdx = 0; instIdx < sampleInsts.numInstances(); instIdx++)
            {
                var sampleInst = sampleInsts.instance(instIdx);
                var cloneInst = cloneInsts.instance(instIdx);
                for (int attrIdx = 0; attrIdx < sampleInsts.numAttributes(); attrIdx++)
                {
                    Assert.AreEqual(sampleInst.value(attrIdx), cloneInst.value(attrIdx));
                }
            }
        }
    }
}
